﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MiLibreria1
{
    public partial class ErrorTxtBos : TextBox
    {
        public ErrorTxtBos()
        {
            InitializeComponent();
        }

        public Boolean Validar
        {
            set;
            get;
        }
        public Boolean SoloNumeros
        {
            set;
            get;
        }
    }
}
