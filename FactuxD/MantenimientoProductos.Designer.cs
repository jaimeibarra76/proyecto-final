﻿namespace FactuxD
{
    partial class MantenimientoProductos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIDPro = new System.Windows.Forms.Label();
            this.lblDescri = new System.Windows.Forms.Label();
            this.lblPrecio = new System.Windows.Forms.Label();
            this.txtPrecio = new MiLibreria1.ErrorTxtBos();
            this.txtDescripcion = new MiLibreria1.ErrorTxtBos();
            this.txtIDPro = new MiLibreria1.ErrorTxtBos();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(371, 45);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(371, 106);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(371, 161);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(371, 217);
            // 
            // btnSalir
            // 
            this.btnSalir.Location = new System.Drawing.Point(371, 271);
            // 
            // lblIDPro
            // 
            this.lblIDPro.AutoSize = true;
            this.lblIDPro.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIDPro.Location = new System.Drawing.Point(24, 80);
            this.lblIDPro.Name = "lblIDPro";
            this.lblIDPro.Size = new System.Drawing.Size(82, 13);
            this.lblIDPro.TabIndex = 8;
            this.lblIDPro.Text = "ID_Producto:";
            // 
            // lblDescri
            // 
            this.lblDescri.AutoSize = true;
            this.lblDescri.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescri.Location = new System.Drawing.Point(24, 142);
            this.lblDescri.Name = "lblDescri";
            this.lblDescri.Size = new System.Drawing.Size(78, 13);
            this.lblDescri.TabIndex = 9;
            this.lblDescri.Text = "Descripción:";
            // 
            // lblPrecio
            // 
            this.lblPrecio.AutoSize = true;
            this.lblPrecio.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrecio.Location = new System.Drawing.Point(24, 201);
            this.lblPrecio.Name = "lblPrecio";
            this.lblPrecio.Size = new System.Drawing.Size(47, 13);
            this.lblPrecio.TabIndex = 10;
            this.lblPrecio.Text = "Precio;";
            // 
            // txtPrecio
            // 
            this.txtPrecio.Location = new System.Drawing.Point(125, 198);
            this.txtPrecio.Name = "txtPrecio";
            this.txtPrecio.Size = new System.Drawing.Size(170, 20);
            this.txtPrecio.SoloNumeros = true;
            this.txtPrecio.TabIndex = 13;
            this.txtPrecio.Validar = true;
            // 
            // txtDescripcion
            // 
            this.txtDescripcion.Location = new System.Drawing.Point(125, 139);
            this.txtDescripcion.Name = "txtDescripcion";
            this.txtDescripcion.Size = new System.Drawing.Size(170, 20);
            this.txtDescripcion.SoloNumeros = false;
            this.txtDescripcion.TabIndex = 12;
            this.txtDescripcion.Validar = true;
            // 
            // txtIDPro
            // 
            this.txtIDPro.Location = new System.Drawing.Point(125, 80);
            this.txtIDPro.Name = "txtIDPro";
            this.txtIDPro.Size = new System.Drawing.Size(170, 20);
            this.txtIDPro.SoloNumeros = true;
            this.txtIDPro.TabIndex = 11;
            this.txtIDPro.Validar = true;
            this.txtIDPro.TextChanged += new System.EventHandler(this.txtIDPro_TextChanged_1);
            // 
            // MantenimientoProductos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(529, 349);
            this.Controls.Add(this.txtPrecio);
            this.Controls.Add(this.txtDescripcion);
            this.Controls.Add(this.txtIDPro);
            this.Controls.Add(this.lblPrecio);
            this.Controls.Add(this.lblDescri);
            this.Controls.Add(this.lblIDPro);
            this.Name = "MantenimientoProductos";
            this.Text = "MantenimientoProductos";
            this.Controls.SetChildIndex(this.btnSalir, 0);
            this.Controls.SetChildIndex(this.button1, 0);
            this.Controls.SetChildIndex(this.button2, 0);
            this.Controls.SetChildIndex(this.button3, 0);
            this.Controls.SetChildIndex(this.button4, 0);
            this.Controls.SetChildIndex(this.lblIDPro, 0);
            this.Controls.SetChildIndex(this.lblDescri, 0);
            this.Controls.SetChildIndex(this.lblPrecio, 0);
            this.Controls.SetChildIndex(this.txtIDPro, 0);
            this.Controls.SetChildIndex(this.txtDescripcion, 0);
            this.Controls.SetChildIndex(this.txtPrecio, 0);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblIDPro;
        private System.Windows.Forms.Label lblDescri;
        private System.Windows.Forms.Label lblPrecio;
        private MiLibreria1.ErrorTxtBos txtIDPro;
        private MiLibreria1.ErrorTxtBos txtDescripcion;
        private MiLibreria1.ErrorTxtBos txtPrecio;
    }
}